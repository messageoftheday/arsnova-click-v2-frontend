import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMotdComponent } from './admin-motd.component';

describe('AdminMotdComponent', () => {
  let component: AdminMotdComponent;
  let fixture: ComponentFixture<AdminMotdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminMotdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMotdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
