import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MotdEntity} from '../../lib/entities/motd/MotdEntity';
import {AdminApiService} from '../../service/api/admin/admin-api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EditMotdModalComponent} from '../../modals/edit-motd-modal/edit-motd-modal.component';


import {MarkdownService} from 'ngx-markdown';
import {MotdDataService} from '../../service/motd/motd-data.service';
import {DatePipe} from '@angular/common';


@Component({
    selector: 'app-admin-motd',
    templateUrl: './admin-motd.component.html',
    styleUrls: ['./admin-motd.component.scss']
})
export class AdminMotdComponent implements OnInit {

    public motdForm: FormGroup;
    public isSending = false;
    public compiledMDContent: String;
    @ViewChild('textarea', {static: true}) private textarea: ElementRef<HTMLTextAreaElement>;

    constructor(private fb: FormBuilder,
                private adminApiService: AdminApiService,
                public motdData: MotdDataService,
                private ngbModal: NgbModal,
                private markdownService: MarkdownService,
                private datePipe: DatePipe
    ) {
    }

    ngOnInit(): void {
        this.motdForm = this.fb.group({
            title: ['', Validators.required],
            content: ['', Validators.required],
            isPinned: [false],
            expireAt: [this.datePipe.transform(new Date(Date.now() + 1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 30), 'yyyy-MM-dd'), Validators.required],
        }, {updateOn: 'change'});
    }

    public submitForm(): void {
        this.isSending = true;
        for (const i in this.motdForm.controls) {
            this.motdForm.controls[i].markAsDirty();
            this.motdForm.controls[i].updateValueAndValidity();
            if (this.motdForm.controls[i].invalid) {
                console.log(i);
                console.log(this.motdForm.controls[i].value);
            }
        }

        if (this.motdForm.valid) {
            const newMotd = new MotdEntity(this.motdForm.get('title').value, this.motdForm.get('content').value, this.motdForm.get('isPinned').value, this.motdForm.get('expireAt').value);
            this.adminApiService.createMotd(newMotd).subscribe(motd => {
                if (motd) {
                    this.motdData.allMotds = [...this.motdData.allMotds, motd];
                    this.motdForm.reset();
                } else {
                    console.log('error creating motd');
                }
                this.isSending = false;
            });
        }
        this.isSending = false;
    }

    public deleteMotd(motdId: String): void {
        this.adminApiService.deleteMotd(motdId).subscribe(res => {
            if (res) {
                this.motdData.allMotds = this.motdData.allMotds.filter(m => m._id !== motdId);
            }
        });
    }

    public startEditMotd(motd: MotdEntity): void {

        const editMotdRef = this.ngbModal.open(EditMotdModalComponent);
        editMotdRef.componentInstance.motd = motd;
        editMotdRef.componentInstance.createForm();
    }

    public compileMD(motdContent: string): string {
        return this.markdownService.compile(motdContent);
    }

    public getMarkdownPreview(): string {
        return this.markdownService.compile(this.motdForm.get('content').value);
    }

    public isPinnedChange(status: boolean): void {
        console.log(status);
        if (status) {
            // Set pinned message to expire in 1000 days
            this.motdForm.get('expireAt').setValue(this.datePipe.transform(new Date(Date.now() + 1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 1000), 'yyyy-MM-dd'));
        } else {
            this.motdForm.get('expireAt').setValue(this.datePipe.transform(new Date(Date.now() + 1000 /*sec*/ * 60 /*min*/ * 60 /*hour*/ * 24 /*day*/ * 30), 'yyyy-MM-dd'));

        }
    }
}
