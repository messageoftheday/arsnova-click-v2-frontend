import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGitSettingsFormComponent } from './admin-git-settings-form.component';

describe('AdminGitSettingsFormComponent', () => {
  let component: AdminGitSettingsFormComponent;
  let fixture: ComponentFixture<AdminGitSettingsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminGitSettingsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGitSettingsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
