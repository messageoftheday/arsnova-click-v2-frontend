import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AdminApiService} from '../../../service/api/admin/admin-api.service';
import {SettingsService} from '../../../service/settings/settings.service';
import {SettingsApiService} from '../../../service/settings/settings-api.service';

@Component({
  selector: 'app-admin-git-settings-form',
  templateUrl: './admin-git-settings-form.component.html',
  styleUrls: ['./admin-git-settings-form.component.scss']
})
export class AdminGitSettingsFormComponent implements OnInit {
  public settingsForm: FormGroup;
  public settings: any;
  public isSending = false;

  constructor(private fb: FormBuilder,
              private adminApiService: AdminApiService,
              private settingsService: SettingsService,
              private settingsApi: SettingsApiService) {
  }

  ngOnInit(): void {
    this.settingsApi.getSettingsByName('git').then(settings => {
      if(settings) {
        console.log(settings);
        this.settings = settings;
        this.settingsForm = this.fb.group({
          frontendGitlabId: [settings.config.frontendGitlabId, [Validators.required]],
          backendGitlabId: [settings.config.backendGitlabId, [Validators.required]],
          gitlabLoginToken: [settings.config.gitlabLoginToken, [Validators.required]],
          gitlabHost: [settings.config.gitlabHost, [Validators.required]],
          gitlabTargetBranch: [settings.config.gitlabTargetBranch, [Validators.required]],
        });
      } else {
        console.log('no settings received');
      }
    });
  }

  async submitForm(): Promise<void> {
    this.isSending = true;
    for (const i in this.settingsForm.controls) {
      this.settingsForm.controls[i].markAsDirty();
      this.settingsForm.controls[i].updateValueAndValidity();
      if (this.settingsForm.controls[i].invalid) {
        console.log(i);
        console.log(this.settingsForm.controls[i].value);
      }
    }

    if(this.settingsForm.valid) {
      this.settings.config.frontendGitlabId = this.settingsForm.get('frontendGitlabId').value;
      this.settings.config.backendGitlabId = this.settingsForm.get('backendGitlabId').value;
      this.settings.config.gitlabLoginToken = this.settingsForm.get('gitlabLoginToken').value;
      this.settings.config.gitlabHost = this.settingsForm.get('gitlabHost').value;
      this.settings.config.gitlabTargetBranch = this.settingsForm.get('gitlabTargetBranch').value;
      this.adminApiService.editSettings(this.settings).subscribe(res => {
        if (res) {
          console.log(res);
          this.settingsService.twitterSettings = res.config;
        } else {
          console.log('error udpating settings');
        }
        this.isSending = false;
      });
    }
  }


}
