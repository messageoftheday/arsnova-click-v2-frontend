import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminMiscSettingsFormComponent } from './admin-misc-settings-form.component';

describe('AdminMiscSettingsFormComponent', () => {
  let component: AdminMiscSettingsFormComponent;
  let fixture: ComponentFixture<AdminMiscSettingsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminMiscSettingsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminMiscSettingsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
