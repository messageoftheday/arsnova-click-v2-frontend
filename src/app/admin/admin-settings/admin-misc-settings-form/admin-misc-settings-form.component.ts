import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AdminApiService} from '../../../service/api/admin/admin-api.service';
import {SettingsService} from '../../../service/settings/settings.service';
import {SettingsApiService} from '../../../service/settings/settings-api.service';

@Component({
  selector: 'app-admin-misc-settings-form',
  templateUrl: './admin-misc-settings-form.component.html',
  styleUrls: ['./admin-misc-settings-form.component.scss']
})
export class AdminMiscSettingsFormComponent implements OnInit {

  public settingsForm: FormGroup;
  public settings: any;
  public isSending = false;

  constructor(private fb: FormBuilder,
              private adminApiService: AdminApiService,
              private settingsService: SettingsService,
              private settingsApi: SettingsApiService) {
  }

  ngOnInit(): void {
    this.settingsApi.getSettingsByName('misc').then(settings => {
      if(settings) {
        console.log(settings);
        this.settings = settings;
        this.settingsForm = this.fb.group({
          chromiumPath: [settings.config.chromiumPath, [Validators.required]],
          projectEmail: [settings.config.projectEmail, [Validators.required]],
          logoFilename: [settings.config.logoFilename, [Validators.required]],
        });
      } else {
        console.log('no settings received');
      }
    });
  }

  async submitForm(): Promise<void> {
    this.isSending = true;
    for (const i in this.settingsForm.controls) {
      this.settingsForm.controls[i].markAsDirty();
      this.settingsForm.controls[i].updateValueAndValidity();
      if (this.settingsForm.controls[i].invalid) {
        console.log(i);
        console.log(this.settingsForm.controls[i].value);
      }
    }

    if(this.settingsForm.valid) {
      this.settings.config.chromiumPath = this.settingsForm.get('chromiumPath').value;
      this.settings.config.projectEmail = this.settingsForm.get('projectEmail').value;
      this.settings.config.logoFilename = this.settingsForm.get('logoFilename').value;
      this.adminApiService.editSettings(this.settings).subscribe(res => {
        if (res) {
          console.log(res);
          this.settingsService.twitterSettings = res.config;
        } else {
          console.log('error udpating settings');
        }
        this.isSending = false;
      });
    }
  }


}
