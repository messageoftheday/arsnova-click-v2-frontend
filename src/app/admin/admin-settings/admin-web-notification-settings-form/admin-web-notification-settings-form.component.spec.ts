import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminWebNotificationSettingsFormComponent } from './admin-web-notification-settings-form.component';

describe('AdminWebNotificationSettingsFormComponent', () => {
  let component: AdminWebNotificationSettingsFormComponent;
  let fixture: ComponentFixture<AdminWebNotificationSettingsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminWebNotificationSettingsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminWebNotificationSettingsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
