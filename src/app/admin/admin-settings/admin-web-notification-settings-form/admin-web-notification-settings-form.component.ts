import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AdminApiService} from '../../../service/api/admin/admin-api.service';
import {SettingsService} from '../../../service/settings/settings.service';
import {SettingsApiService} from '../../../service/settings/settings-api.service';

@Component({
  selector: 'app-admin-web-notification-settings-form',
  templateUrl: './admin-web-notification-settings-form.component.html',
  styleUrls: ['./admin-web-notification-settings-form.component.scss']
})
export class AdminWebNotificationSettingsFormComponent implements OnInit {

  public settingsForm: FormGroup;
  public settings: any;
  public isSending = false;

  constructor(private fb: FormBuilder,
              private adminApiService: AdminApiService,
              private settingsService: SettingsService,
              private settingsApi: SettingsApiService) {
  }

  ngOnInit(): void {
    this.settingsApi.getSettingsByName('web_notification').then(settings => {
      if(settings) {
        console.log(settings);
        this.settings = settings;
        this.settingsForm = this.fb.group({
          vapidPublicKey: [settings.config.vapidPublicKey, [Validators.required]],
          vapidPrivateKey: [settings.config.vapidPrivateKey, [Validators.required]],
        });
      } else {
        console.log('no settings received');
      }
    });
  }

  async submitForm(): Promise<void> {
    this.isSending = true;
    for (const i in this.settingsForm.controls) {
      this.settingsForm.controls[i].markAsDirty();
      this.settingsForm.controls[i].updateValueAndValidity();
      if (this.settingsForm.controls[i].invalid) {
        console.log(i);
        console.log(this.settingsForm.controls[i].value);
      }
    }

    if(this.settingsForm.valid) {
      this.settings.config.vapidPublicKey = this.settingsForm.get('vapidPublicKey').value;
      this.settings.config.vapidPrivateKey = this.settingsForm.get('vapidPrivateKey').value;
      this.adminApiService.editSettings(this.settings).subscribe(res => {
        if (res) {
          console.log(res);
          this.settingsService.twitterSettings = res.config;
        } else {
          console.log('error udpating settings');
        }
        this.isSending = false;
      });
    }
  }
}
