import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontEnvSettingsFormComponent } from './front-env-settings-form.component';

describe('FrontEnvSettingsFormComponent', () => {
  let component: FrontEnvSettingsFormComponent;
  let fixture: ComponentFixture<FrontEnvSettingsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontEnvSettingsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontEnvSettingsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
