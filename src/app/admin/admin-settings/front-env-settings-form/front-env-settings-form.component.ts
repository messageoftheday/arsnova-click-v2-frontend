import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AdminApiService} from '../../../service/api/admin/admin-api.service';
import {SettingsApiService} from '../../../service/settings/settings-api.service';
import {SettingsService} from '../../../service/settings/settings.service';

@Component({
    selector: 'app-front-env-settings-form',
    templateUrl: './front-env-settings-form.component.html',
    styleUrls: ['./front-env-settings-form.component.scss'],
})
export class FrontEnvSettingsFormComponent implements OnInit {
    public settingsForm: FormGroup;
    public settings: any;
    public isSending = false;
    public leaderBoardAlgorithms: Array<String> = ['PointBased', 'TimeBased'];
    public loginMechanisms: Array<{ id: number, name: string }> = [
        {id: 0, name: 'UsernamePassword'},
        {id: 1, name: 'Token'}
    ];
    public selectableThemes: Array<String> = [
        'WestermannBlue',
        'Blackbeauty'
    ];

    constructor(
        private fb: FormBuilder,
        private adminApiService: AdminApiService,
        private settingsService: SettingsService,
        private settingsApi: SettingsApiService
    ) {
    }

    ngOnInit(): void {
        this.settingsApi.getSettingsByName('front_env').then((settings) => {
            if (settings) {
                console.log(settings);
                this.settings = settings;
                this.settingsForm = this.fb.group({
                    envEnabled: [
                        settings.config.envEnabled,
                    ],
                    title: [settings.config.title,
                        [Validators.required]
                    ],
                    appName: [settings.config.appName,
                        [Validators.required]
                    ],
                    version: [settings.config.version,
                        [Validators.required]
                    ],
                    sentryDSN: [settings.config.sentryDSN,
                        [Validators.required]
                    ],
                    leaderboardAlgorithm: [
                        settings.config.leaderboardAlgorithm,
                        [Validators.required],
                    ],
                    leaderboardAmount: [
                        settings.config.leaderboardAmount,
                        [Validators.required],
                    ],
                    enableBonusToken: [
                        settings.config.enableBonusToken,
                        [Validators.required],
                    ],
                    readingConfirmationEnabled: [
                        settings.config.readingConfirmationEnabled,
                        [Validators.required],
                    ],
                    confidenceSliderEnabled: [
                        settings.config.confidenceSliderEnabled,
                        [Validators.required],
                    ],
                    infoAboutTabEnabled: [
                        settings.config.infoAboutTabEnabled,
                        [Validators.required],
                    ],
                    infoProjectTabEnabled: [
                        settings.config.infoProjectTabEnabled,
                        [Validators.required],
                    ],
                    infoBackendApiEnabled: [
                        settings.config.infoBackendApiEnabled,
                        [Validators.required],
                    ],
                    requireLoginToCreateQuiz: [
                        settings.config.requireLoginToCreateQuiz,
                        [Validators.required],
                    ],
                    forceQuizTheme: [
                        settings.config.forceQuizTheme,
                        [Validators.required],
                    ],
                    loginMechanism: [
                        settings.config.loginMechanism,
                        [Validators.required],
                    ],
                    showLoginButton: [
                        settings.config.showLoginButton,
                        [Validators.required],
                    ],
                    showJoinableQuizzes: [
                        settings.config.showJoinableQuizzes,
                        [Validators.required],
                    ],
                    showPublicQuizzes: [
                        settings.config.showPublicQuizzes,
                        [Validators.required],
                    ],
                    persistQuizzes: [
                        settings.config.persistQuizzes,
                        [Validators.required],
                    ],
                    availableQuizThemes: [
                        settings.config.availableQuizThemes,
                        [Validators.required],
                    ],
                    defaultTheme: [settings.config.defaultTheme, [Validators.required]],
                    darkModeCheckEnabled: [
                        settings.config.darkModeCheckEnabled,
                        [Validators.required],
                    ],
                    enableTwitter: [settings.config.enableTwitter, [Validators.required]],
                    enableQuizPool: [
                        settings.config.enableQuizPool,
                        [Validators.required],
                    ],
                    showInfoButtonsInFooter: [
                        settings.config.showInfoButtonsInFooter,
                        [Validators.required],
                    ],
                    vapidPublicKey: [
                        settings.config.vapidPublicKey,
                        [Validators.required],
                    ],
                    markdownFilePostfix: [
                        settings.config.markdownFilePostfix,
                        [Validators.required],
                    ],
                    loginButtonLabelConfiguration: [
                        settings.config.loginButtonLabelConfiguration,
                        [Validators.required],
                    ],
                });
            } else {
                console.log('no settings received');
            }
        });
    }

    async submitForm(): Promise<void> {
        this.isSending = true;
        for (const i in this.settingsForm.controls) {
            this.settingsForm.controls[i].markAsDirty();
            this.settingsForm.controls[i].updateValueAndValidity();
            if (this.settingsForm.controls[i].invalid) {
                console.log(i);
                console.log(this.settingsForm.controls[i].value);
            }
        }

        if (this.settingsForm.valid) {
            this.settings.config.envEnabled = this.settingsForm.get('envEnabled').value;
            this.settings.config.title = this.settingsForm.get('title').value;
            this.settings.config.appName = this.settingsForm.get('appName').value;
            this.settings.config.version = this.settingsForm.get('version').value;
            this.settings.config.sentryDSN = this.settingsForm.get('sentryDSN').value;
            this.settings.config.leaderboardAlgorithm = this.settingsForm.get(
                'leaderboardAlgorithm'
            ).value;
            this.settings.config.leaderboardAmount = this.settingsForm.get(
                'leaderboardAmount'
            ).value;
            this.settings.config.enableBonusToken = this.settingsForm.get(
                'enableBonusToken'
            ).value;
            this.settings.config.readingConfirmationEnabled = this.settingsForm.get(
                'readingConfirmationEnabled'
            ).value;
            this.settings.config.confidenceSliderEnabled = this.settingsForm.get(
                'confidenceSliderEnabled'
            ).value;
            this.settings.config.infoAboutTabEnabled = this.settingsForm.get(
                'infoAboutTabEnabled'
            ).value;
            this.settings.config.infoProjectTabEnabled = this.settingsForm.get(
                'infoProjectTabEnabled'
            ).value;
            this.settings.config.infoBackendApiEnabled = this.settingsForm.get(
                'infoBackendApiEnabled'
            ).value;
            this.settings.config.requireLoginToCreateQuiz = this.settingsForm.get(
                'requireLoginToCreateQuiz'
            ).value;
            this.settings.config.forceQuizTheme = this.settingsForm.get(
                'forceQuizTheme'
            ).value;
            this.settings.config.loginMechanism = this.settingsForm.get(
                'loginMechanism'
            ).value;
            this.settings.config.showLoginButton = this.settingsForm.get(
                'showLoginButton'
            ).value;
            this.settings.config.showJoinableQuizzes = this.settingsForm.get(
                'showJoinableQuizzes'
            ).value;
            this.settings.config.showPublicQuizzes = this.settingsForm.get(
                'showPublicQuizzes'
            ).value;
            this.settings.config.persistQuizzes = this.settingsForm.get(
                'persistQuizzes'
            ).value;
            this.settings.config.availableQuizThemes = this.settingsForm.get(
                'availableQuizThemes'
            ).value;
            this.settings.config.defaultTheme = this.settingsForm.get(
                'defaultTheme'
            ).value;
            this.settings.config.darkModeCheckEnabled = this.settingsForm.get(
                'darkModeCheckEnabled'
            ).value;
            this.settings.config.enableTwitter = this.settingsForm.get(
                'enableTwitter'
            ).value;
            this.settings.config.enableQuizPool = this.settingsForm.get(
                'enableQuizPool'
            ).value;
            this.settings.config.showInfoButtonsInFooter = this.settingsForm.get(
                'showInfoButtonsInFooter'
            ).value;
            this.settings.config.vapidPublicKey = this.settingsForm.get(
                'vapidPublicKey'
            ).value;
            this.settings.config.markdownFilePostfix = this.settingsForm.get(
                'markdownFilePostfix'
            ).value;
            this.settings.config.loginButtonLabelConfiguration = this.settingsForm.get(
                'loginButtonLabelConfiguration'
            ).value;

            this.adminApiService.editSettings(this.settings).subscribe((res) => {
                if (res) {
                    console.log(res);
                    this.settingsService.frontEnv = res.config;
                } else {
                    console.log('error udpating settings');
                }
                this.isSending = false;
            });
        }
        this.isSending = false;
    }
}
