import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TwitterSettingsFormComponent } from './twitter-settings-form.component';

describe('TwitterSettingsFormComponent', () => {
  let component: TwitterSettingsFormComponent;
  let fixture: ComponentFixture<TwitterSettingsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TwitterSettingsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TwitterSettingsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
