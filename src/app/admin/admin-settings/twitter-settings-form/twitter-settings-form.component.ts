import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AdminApiService} from '../../../service/api/admin/admin-api.service';
import {SettingsService} from '../../../service/settings/settings.service';
import {SettingsApiService} from '../../../service/settings/settings-api.service';

@Component({
    selector: 'app-twitter-settings-form',
    templateUrl: './twitter-settings-form.component.html',
    styleUrls: ['./twitter-settings-form.component.scss']
})
export class TwitterSettingsFormComponent implements OnInit {
    public settingsForm: FormGroup;
    public settings: any;
    public isSending = false;

    constructor(private fb: FormBuilder,
                private adminApiService: AdminApiService,
                private settingsService: SettingsService,
                private settingsApi: SettingsApiService) {
    }

    ngOnInit(): void {
        this.settingsApi.getSettingsByName('twitter').then(settings => {
            if(settings) {
                console.log(settings);
              this.settings = settings;
              this.settingsForm = this.fb.group({
                twitterEnabled: [settings.config.twitterEnabled, [Validators.required]],
                twitterConsumerKey: [settings.config.twitterConsumerKey, [Validators.required]],
                twitterConsumerSecret: [settings.config.twitterConsumerSecret, [Validators.required]],
                twitterBearerToken: [settings.config.twitterBearerToken, [Validators.required]],
                twitterAccessTokenKey: [settings.config.twitterAccessTokenKey, [Validators.required]],
                twitterSearchKey: [settings.config.twitterSearchKey, [Validators.required]],
              });
            } else {
              console.log('no settings received');
            }
        });
    }

    async submitForm(): Promise<void> {
        this.isSending = true;
        for (const i in this.settingsForm.controls) {
            this.settingsForm.controls[i].markAsDirty();
            this.settingsForm.controls[i].updateValueAndValidity();
            if (this.settingsForm.controls[i].invalid) {
                console.log(i);
                console.log(this.settingsForm.controls[i].value);
            }
        }

        if(this.settingsForm.valid) {
            this.settings.config.twitterEnabled = this.settingsForm.get('twitterEnabled').value;
            this.settings.config.twitterConsumerKey = this.settingsForm.get('twitterConsumerKey').value;
            this.settings.config.twitterConsumerSecret = this.settingsForm.get('twitterConsumerSecret').value;
            this.settings.config.twitterBearerToken = this.settingsForm.get('twitterBearerToken').value;
            this.settings.config.twitterAccessTokenKey = this.settingsForm.get('twitterAccessTokenKey').value;
            this.settings.config.twitterSearchKey = this.settingsForm.get('twitterSearchKey').value;
            this.adminApiService.editSettings(this.settings).subscribe(res => {
                if (res) {
                    console.log(res);
                    this.settingsService.twitterSettings = res.config;
                } else {
                    console.log('error udpating settings');
                }
                this.isSending = false;
            });
        }
    }
}

