export class MotdEntity {
    public _id?: String; 
    public mid: number;
    public title: string;
    public isPinned: boolean;
    public content: string;
    public createdAt: Date;
    public updatedAt: Date;
    public expireAt: Date;

    constructor(title: string, content: string, isPinned: boolean, expiryAt: Date) {
        this.title = title;
        this.content = content;
        this.isPinned = isPinned;
        this.expireAt = expiryAt;
    }
}
