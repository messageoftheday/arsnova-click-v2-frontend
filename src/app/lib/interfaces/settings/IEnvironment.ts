import {QuizTheme} from '../../enums/QuizTheme';

export interface IEnvironment {
    enableBonusToken: boolean;
    

    vapidPublicKey: string;
    title: string;
    appName: string;
    sentryDSN?: string;
    version: string;
    production: boolean;
    stompConfig: {
      endpoint: string, user: string, password: string, vhost: string,
    };

    leaderboardAlgorithm: string;
    leaderboardAmount: number;

    readingConfirmationEnabled: boolean;
    confidenceSliderEnabled: boolean;
    infoAboutTabEnabled: boolean;
    infoProjectTabEnabled: boolean;
    infoBackendApiEnabled: boolean;
    requireLoginToCreateQuiz: boolean;
    showJoinableQuizzes: boolean;
    showPublicQuizzes: boolean;
    forceQuizTheme: boolean;
    loginMechanism: Array<Number>;
    showLoginButton: boolean;
    persistQuizzes: boolean;
    availableQuizThemes: Array<QuizTheme>;
    defaultTheme: QuizTheme;
    darkModeCheckEnabled: boolean;
    enableTwitter: boolean;
    enableQuizPool: boolean;
    showInfoButtonsInFooter: boolean;
    markdownFilePostfix: string;
    loginButtonLabelConfiguration: string;
}
