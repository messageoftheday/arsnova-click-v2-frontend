export interface ITwitter {
    twitterEnabled: boolean;
    twitterConsumerKey: string;
    twitterConsumerSecret: string;
    twitterBearerToken: string;
    twitterAccessTokenKey: string;
    twitterAccessTokenSecret: string;
    twitterSearchKey: string;

}
