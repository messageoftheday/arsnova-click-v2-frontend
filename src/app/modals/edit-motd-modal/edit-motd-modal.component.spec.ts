import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMotdModalComponent } from './edit-motd-modal.component';

describe('EditMotdModalComponent', () => {
  let component: EditMotdModalComponent;
  let fixture: ComponentFixture<EditMotdModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMotdModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMotdModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
