import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MotdModalComponent } from './motd-modal.component';

describe('MotdModalComponent', () => {
  let component: MotdModalComponent;
  let fixture: ComponentFixture<MotdModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MotdModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MotdModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
