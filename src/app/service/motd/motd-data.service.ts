import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {MotdEntity} from '../../lib/entities/motd/MotdEntity';
import {isPlatformBrowser} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class MotdDataService {

  public allMotds: MotdEntity[] = [];
  public unseenMotds: MotdEntity[] = [];

  constructor(@Inject(PLATFORM_ID) private platformId) { }

  public saveLastSeenMotdId(motdId: number): void {
    console.log('saveLastSeenMotdId', motdId);
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem('lastSeenMotdId', motdId.toString());
    }
  }

  public getLastSeenMotdId(): number {
    if (isPlatformBrowser(this.platformId)) {
      if (!localStorage.getItem('lastSeenMotdId')) {
        return 0;
      }
      return parseInt(localStorage.getItem('lastSeenMotdId'), 10);
    }
  }

  public getUnseenMotds(): MotdEntity[] {
    const lastSeenMotdId = this.getLastSeenMotdId();
    return this.allMotds?.filter(m => m.isPinned || m.mid > lastSeenMotdId);
  }
}
