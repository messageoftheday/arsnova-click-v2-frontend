import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MotdEntity} from '../../lib/entities/motd/MotdEntity';
import {DefaultSettings} from '../../lib/default.settings';
import {Observable} from 'rxjs/index';
import {isPlatformBrowser} from '@angular/common';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MotdModalComponent} from '../../modals/motd-modal/motd-modal.component';
import {MotdDataService} from './motd-data.service';

@Injectable({
    providedIn: 'root'
})
export class MotdService {


    constructor(private http: HttpClient, @Inject(PLATFORM_ID) private platformId, private ngbModal: NgbModal, private motdData: MotdDataService) {

    }

    public getAllMotds(): Observable<MotdEntity[]> {
        return this.http.get<MotdEntity[]>(`${DefaultSettings.httpApiEndpoint}/admin/motd`);
    }


    public showMotdModal(force = false): Promise<void> {
        if (this.motdData.allMotds || force) {

            this.motdData.unseenMotds = this.motdData.getUnseenMotds();
            if (this.motdData.unseenMotds?.length > 0 || force) {
                if (this.motdData.unseenMotds?.length > 0) {
                    this.motdData.saveLastSeenMotdId(this.motdData.allMotds.sort((a, b) => b.mid - a.mid)[0].mid);
                }
                return this.ngbModal.open(MotdModalComponent, {size: 'lg'}).result.then(() => {
                }).catch(() => {
                });
            }
        }
    }


}
