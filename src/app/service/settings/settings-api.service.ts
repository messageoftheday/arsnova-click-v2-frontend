import { Injectable } from '@angular/core';
import {DefaultSettings} from '../../lib/default.settings';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SettingsApiService {



  constructor(private http: HttpClient) {

  }

  public async getSettingsByName(name: string): Promise<any> {
    return await this.http.get<any>(
        `${DefaultSettings.httpApiEndpoint}/admin/settings/` + name,
    ).toPromise().then(settings => {
      return {
        _id: settings._id,
        configName: settings.configName,
        config: settings.config
      };
    });
  }


}
