import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { IServerSettings } from '../../lib/interfaces/IServerSettings';
import {ITwitter} from '../../lib/interfaces/settings/ITwitter';
import {IEnvironment} from '../../lib/interfaces/settings/IEnvironment';
import {IGit} from '../../lib/interfaces/settings/IGit';
import {IMiscellaneous} from '../../lib/interfaces/settings/IMiscellaneous';
import {IWebNotifications} from '../../lib/interfaces/settings/IWebNotifications';

@Injectable({
  providedIn: 'root',
})
export class SettingsMockService {
  private _serverSettings: IServerSettings;
  public twitterSettings: ITwitter;
  public frontEnv: IEnvironment;
  public gitSettings: IGit;
  public misc: IMiscellaneous;
  public webNotificationSettings: IWebNotifications;
  get serverSettings(): IServerSettings {
    return this._serverSettings;
  }

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
  }

  private async initServerSettings(): Promise<void> {
    return new Promise<void>(resolve => resolve());
  }
}
