import {isPlatformBrowser} from '@angular/common';
import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {IServerSettings} from '../../lib/interfaces/IServerSettings';
import {ConnectionService} from '../connection/connection.service';
import {SharedService} from '../shared/shared.service';
import {BehaviorSubject} from 'rxjs';
import {SettingsApiService} from './settings-api.service';
import {IMiscellaneous} from '../../lib/interfaces/settings/IMiscellaneous';
import {IWebNotifications} from '../../lib/interfaces/settings/IWebNotifications';
import {IGit} from '../../lib/interfaces/settings/IGit';
import {ITwitter} from '../../lib/interfaces/settings/ITwitter';
import {IEnvironment} from '../../lib/interfaces/settings/IEnvironment';

@Injectable({
    providedIn: 'root',
})
export class SettingsService {

    public settingsLoadedSubject = new BehaviorSubject<boolean>(false);

    public finishedLoadingSettings = false;
    public twitterSettings: ITwitter;
    public frontEnv: IEnvironment;
    public gitSettings: IGit;
    public misc: IMiscellaneous;
    public webNotificationSettings: IWebNotifications;

    constructor(@Inject(PLATFORM_ID) private platformId: Object, private connectionService: ConnectionService, private sharedService: SharedService, private settingsApi: SettingsApiService) {
        if (isPlatformBrowser(this.platformId)) {
            this.initServerSettings();
            this.initServerSideLoadedSettings();
        }
    }

    private _serverSettings: IServerSettings;

    get serverSettings(): IServerSettings {
        return this._serverSettings;
    }

    private async initServerSideLoadedSettings(): Promise<void> {
        this.twitterSettings = (await this.settingsApi.getSettingsByName('twitter'))?.config;
        this.frontEnv = (await this.settingsApi.getSettingsByName('front_env'))?.config;
        this.gitSettings = (await this.settingsApi.getSettingsByName('git'))?.config;
        this.misc = (await this.settingsApi.getSettingsByName('misc'))?.config;
        this.webNotificationSettings = (await this.settingsApi.getSettingsByName('web_notification'))?.config;
        this.finishedLoadingSettings = true;
        this.settingsLoadedSubject.next(true);
    }

    private async initServerSettings(): Promise<void> {
        const data = await this.connectionService.initConnection(true);


        if (!data) {
            return;
        }

        this._serverSettings = data.serverConfig;
        this.sharedService.activeQuizzes = data.activeQuizzes;

        // Workaround required because JSON serializes Infinity to null which is then deserialized to NaN by EcmaScript
        if (this._serverSettings.limitActiveQuizzes === null) {
            this._serverSettings.limitActiveQuizzes = Infinity;
        }
    }
}
