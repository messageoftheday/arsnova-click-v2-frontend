import {isPlatformBrowser, isPlatformServer} from '@angular/common';
import {EventEmitter, Inject, Injectable, Optional, PLATFORM_ID} from '@angular/core';
import {makeStateKey, StateKey, TransferState} from '@angular/platform-browser';
import {themes} from '../../lib/available-themes';
import {StorageKey} from '../../lib/enums/enums';
import {MessageProtocol, StatusProtocol} from '../../lib/enums/Message';
import {QuizTheme} from '../../lib/enums/QuizTheme';
import {THEME_MAP} from '../../lib/injection-token/theme-map';
import {ITheme, IThemeHashMap} from '../../lib/interfaces/ITheme';
import {ThemesApiService} from '../api/themes/themes-api.service';
import {ConnectionService} from '../connection/connection.service';
import {I18nService} from '../i18n/i18n.service';
import {QuizService} from '../quiz/quiz.service';
import {StorageService} from '../storage/storage.service';
import {SettingsService} from '../settings/settings.service';
import {SettingsApiService} from '../settings/settings-api.service';

@Injectable({
    providedIn: 'root',
})
export class ThemesService {
    public readonly themeChanged: EventEmitter<QuizTheme> = new EventEmitter<QuizTheme>();
    public themeHashes: Array<IThemeHashMap>;

    constructor(
        @Optional() @Inject(THEME_MAP) private themeMap: Array<IThemeHashMap>, //
        @Inject(PLATFORM_ID) private platformId: Object,
        private quizService: QuizService,
        private connectionService: ConnectionService,
        private themesApiService: ThemesApiService,
        private storageService: StorageService,
        private i18nService: I18nService,
        private transferState: TransferState,
        private settingsService: SettingsService,
        private settingsApi: SettingsApiService
    ) {

        settingsService.settingsLoadedSubject.subscribe(isLoaded => {
            if (isLoaded) {
                this._themes = this.settingsService.frontEnv.availableQuizThemes.map(t => QuizTheme[t]).map(t => themes.find(theme => theme.id === t));
                this._defaultTheme = isPlatformServer(this.platformId) ? QuizTheme[this.settingsService.frontEnv.defaultTheme] : //
                    this.settingsService.frontEnv.darkModeCheckEnabled && //
                    window.matchMedia('(prefers-color-scheme: dark)').matches ? //
                        QuizTheme.Blackbeauty : QuizTheme[this.settingsService.frontEnv.defaultTheme];
            }
        });

        const key: StateKey<Array<IThemeHashMap>> = makeStateKey<Array<IThemeHashMap>>('transfer-theme-map');
        if (this.themeMap) {
            this.transferState.set(key, this.themeMap);
        } else {
            this.themeHashes = this.transferState.get<Array<IThemeHashMap>>(key, null);
            console.log('ThemesService: Transferred state and loaded themeHashes', this.themeHashes);
        }

    }

    private _currentTheme: QuizTheme;

    get currentTheme(): QuizTheme {
        return this._currentTheme;
    }

    private _themes: Array<ITheme>;

    get themes(): Array<ITheme> {
        return this._themes;
    }

    private _defaultTheme: QuizTheme;

    get defaultTheme(): QuizTheme {
        return this._defaultTheme;
    }

    public async updateCurrentlyUsedTheme(): Promise<void> {
        if (isPlatformServer(this.platformId)) {
            return;
        }

        await this.settingsApi.getSettingsByName('front_env').then(async settings => {
            this.settingsService.frontEnv = settings.config;
            console.log('themeservice', this.settingsService.frontEnv);

            const themePromises: Array<Promise<any>> = [];
            if (!this.settingsService.frontEnv?.forceQuizTheme || !this.quizService.quiz) {
                themePromises.push(this.storageService.db.Config.get(StorageKey.DefaultTheme), this.storageService.db.Config.get(StorageKey.QuizTheme));
            }
            themePromises.push(new Promise(resolve => {
                if (this.quizService.quiz && this.quizService.quiz.sessionConfig.theme) {
                    resolve(this.quizService.quiz.sessionConfig.theme);
                    return;
                }
                resolve(this._defaultTheme);
            }));

            const themeConfig = await Promise.all(themePromises);
            let usedTheme = themeConfig[0] ? themeConfig[0].value : themeConfig[1] ? themeConfig[1].value : themeConfig[2];
            if (usedTheme === 'default' || !Object.values(this.settingsService.frontEnv?.availableQuizThemes.map(t => QuizTheme[t])).includes(usedTheme)) {
                usedTheme = QuizTheme[this.settingsService.frontEnv?.defaultTheme];
            }
            const themeDataset = document.getElementsByTagName('html').item(0).dataset['theme'];

            if (!document.getElementById('link-manifest') && themeDataset === usedTheme) {
                this.reloadLinkNodes(usedTheme);
            }
            if (themeDataset !== usedTheme) {
                this._currentTheme = usedTheme;
                this.themeChanged.emit(this._currentTheme);
                document.getElementsByTagName('html').item(0).dataset['theme'] = usedTheme;
                this.reloadLinkNodes(usedTheme);
            }
        });
    }

    public reloadLinkNodes(theme?): void {
        if (isPlatformServer(this.platformId) || (!document.getElementById('link-manifest') && !theme)) {
            return;
        }

        console.log('ThemesService: Reloading link nodes');

        if (!theme) {
            theme = this._currentTheme;
        }

        console.log('theme', theme);
        this.themeChanged.emit(theme);

        this.themesApiService.getLinkImages(theme).subscribe(data => {
            data.forEach((elem) => {
                if (elem.id === 'link-manifest') {
                    elem.href = elem.href.replace('%%LANG%%', this.i18nService.currentLanguage.toLowerCase());
                }
                const previousElement = document.getElementById(elem.id);
                if (previousElement) {
                    this.replaceExistingNode(previousElement, elem);
                } else {
                    this.addNewNode(elem);
                }
            });
        }, () => {
        });
    }

    public initTheme(): void {
        if (!isPlatformBrowser(this.platformId)) {
            return;
        }

        this.storageService.db.Config.get(StorageKey.DefaultTheme).then(val => {
            if (!val || val.value?.startsWith('theme-')) {
                // TODO: see next line
                console.log('default theme in theme service?!', this.defaultTheme);
                this.storageService.db.Config.put({
                    value: this.defaultTheme ? this.defaultTheme : [QuizTheme.Material],
                    type: StorageKey.DefaultTheme,
                });
            }
        });

        this.connectionService.dataEmitter.subscribe(data => {
            if (data.status === StatusProtocol.Success && data.step === MessageProtocol.UpdatedSettings) {
                this.quizService.quiz.sessionConfig[data.payload.target] = data.payload.state;
                if (data.payload.target === 'theme') {
                    this.updateCurrentlyUsedTheme();
                }
            }
        });
    }

    private addNewNode(elem): void {
        const child = document.createElement(elem.tagName);
        this.replaceExistingNode(child, elem);
        document.head.appendChild(child);
    }

    private replaceExistingNode(previousElement, elem): void {
        if (elem.className) {
            previousElement.setAttribute('class', elem.className);
        }
        if (elem.type) {
            previousElement.setAttribute('type', elem.type);
        }
        if (elem.id) {
            previousElement.setAttribute('id', elem.id);
        }
        if (elem.rel) {
            previousElement.setAttribute('rel', elem.rel);
        }
        if (elem.href) {
            previousElement.setAttribute('href', elem.href);
        }
        if (elem.name) {
            previousElement.setAttribute('name', elem.name);
        }
        if (elem.content) {
            previousElement.setAttribute('content', elem.content);
        }
    }


}
