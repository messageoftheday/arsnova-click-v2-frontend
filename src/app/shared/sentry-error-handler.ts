import {ErrorHandler, Injectable} from '@angular/core';
import {captureException, Event as SentryEvent, init as SentryInit, setExtra} from '@sentry/browser';
import {EventHint} from '@sentry/types';
import {SettingsService} from '../service/settings/settings.service';

@Injectable({providedIn: 'root'})
export class SentryErrorHandler implements ErrorHandler {

  constructor(private settingsService: SettingsService) {
    settingsService.settingsLoadedSubject.subscribe(isLoaded => {
      console.log('IM HERE', isLoaded);
      if (isLoaded && !!settingsService.frontEnv.sentryDSN) {
        console.log('IM HERE2');
        SentryInit({
          dsn: settingsService.frontEnv.sentryDSN,
          enabled: true,
          release: settingsService.frontEnv.version,
          beforeSend(event: SentryEvent, hint?: EventHint): PromiseLike<SentryEvent | null> | SentryEvent | null {
            if (event.exception.values.some(val => !val.mechanism.handled)) {
              console.log('sending error event', event, hint);
              return event;
            }
            return null;
          },
        });
        setExtra('nonErrorException', false);
      }
    });

  }

  public handleError(error): void {
    captureException(error.originalError || error);
    throw error;
  }
}
