import { LoginMechanism, Title } from '../app/lib/enums/enums';
import { QuizTheme } from '../app/lib/enums/QuizTheme';
import { IEnvironmentClient } from '../app/lib/interfaces/IEnvironmentClient';


export const environment: IEnvironmentClient = {
  production: true,
  serverEndpoint: 'http://localhost:3010',
  
  stompConfig: {
    endpoint: 'ws://localhost:15674/ws',
    user: 'root',
    password: 'pass123',
    vhost: '/',
  },

  //TODO muss geladen werden
  enableBonusToken: true,
  version: '__VERSION__',
  title: Title.Default,
  appName: 'Westermann Quiz',
  sentryDSN: 'https://14415a5e358f4c04b6a878072d352c4e@sentry.arsnova.click/2',
  leaderboardAmount: 5,
  readingConfirmationEnabled: true,
  confidenceSliderEnabled: true,
  infoAboutTabEnabled: true,
  infoProjectTabEnabled: true,
  infoBackendApiEnabled: true,
  requireLoginToCreateQuiz: false,
  forceQuizTheme: false,
  loginMechanism: [LoginMechanism.UsernamePassword],
  showLoginButton: true,
  showJoinableQuizzes: true,
  showPublicQuizzes: true,
  persistQuizzes: true,
  availableQuizThemes: [
    QuizTheme.Material,
    QuizTheme.Blackbeauty,
  ],
  defaultTheme: QuizTheme.Material,
  darkModeCheckEnabled: true,
  enableTwitter: true,
  enableQuizPool: true,
  showInfoButtonsInFooter: false,
  vapidPublicKey: 'BFy8kQxiV2p43Z8Xqs6isn7QRVDEvkqreDH3wH0QlDLDn8cZkbM41iOWwxUBsw_R0Y4Bv8AkI9sKj82P18q41z0',
  markdownFilePostfix: 'westermann',
  loginButtonLabelConfiguration: 'administration',
};
